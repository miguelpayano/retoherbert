package com.changetype.mscurrency.expose;


import com.changetype.mscurrency.expose.request.TypeChangeRequest;
import com.changetype.mscurrency.expose.request.UpdateChangeTypeRequest;
import com.changetype.mscurrency.expose.response.PatchTypeChangeResponse;
import com.changetype.mscurrency.expose.response.TypeChangeResponse;
import com.changetype.mscurrency.expose.response.UpdateChangeTypeResponse;
import com.changetype.mscurrency.service.CurrencyService;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/type-change")
@CrossOrigin
@Slf4j
public class TypeCurrencyChangeController {

    @Autowired
    private CurrencyService currencyService;

    @GetMapping()
    public Observable<UpdateChangeTypeResponse> getAllTypeChange() {
        return currencyService.getAllTypeChange();
    }

    @GetMapping(value = "/{idTypeChange}")
    public Single<UpdateChangeTypeResponse> getTypeChangeById(@PathVariable Integer idTypeChange) {
        return currencyService.findByIdTypeChange(idTypeChange);
    }

    @PutMapping(value = "/{idTypeChange}")
    public Single<UpdateChangeTypeResponse> updateTypeChange(
            @PathVariable Integer idTypeChange,
            @RequestBody UpdateChangeTypeRequest updateChangeTypeRequest) {

        updateChangeTypeRequest.setIdTypeChange(idTypeChange);

        return currencyService.updateTypeChangeById(updateChangeTypeRequest);

    }

    @PatchMapping(value = "/{idTypeChange}")
    public Single<PatchTypeChangeResponse> patchTypeChange(
            @PathVariable Integer idTypeChange,
            @RequestBody UpdateChangeTypeRequest updateChangeTypeRequest) {
        updateChangeTypeRequest.setIdTypeChange(idTypeChange);
            return currencyService.patchTypeChangeById(updateChangeTypeRequest);


    }

    @PostMapping(value="/convert")
    public Single<TypeChangeResponse> postExchangeCurrency(
            @RequestBody TypeChangeRequest typeChangeRequest)
            {
                return currencyService.postTypeChange(typeChangeRequest);

    }


    @PostMapping(value="/save")
    public Single<UpdateChangeTypeResponse> postTypeCurrencyChange(
            @RequestBody UpdateChangeTypeRequest updateChangeTypeRequest) throws Exception
    {
        return currencyService.postTypeCurrencyChange(updateChangeTypeRequest);

    }


}
