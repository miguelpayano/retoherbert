package com.changetype.mscurrency.expose.request;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;

@Getter
@Setter
public class TypeChangeRequest {

    private String originCurrency;
    private String destinationCurrency;
    private BigDecimal amount;

}
