package com.changetype.mscurrency.expose.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatchTypeChangeResponse {
    private Long codigo;
    private String message;
}
