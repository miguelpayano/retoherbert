package com.changetype.mscurrency.expose.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TypeChangeResponse {

    private BigDecimal amount;
    private BigDecimal amountWithExchangeRate;
    private String originCurrency;
    private String destinationCurrency;
    private BigDecimal changeType;
    private String  description;

}
