package com.changetype.mscurrency.expose.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class UpdateChangeTypeResponse {

    private Integer idTypeChange;
    private String description;
    private String originCurrency;
    private String destinationCurrency;
    private BigDecimal changeType;

}
