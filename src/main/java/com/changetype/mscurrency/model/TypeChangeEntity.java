package com.changetype.mscurrency.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "type_change")
public class TypeChangeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type_change")
    private Integer idTypeChange;

    @Column(name = "description")
    private String description;

    @Column(name = "origin_currency")
    private String originCurrency;

    @Column(name = "destination_currency")
    private String destinationCurrency;

    @Column(name = "change_type")
    private BigDecimal changeType;

}
