package com.changetype.mscurrency.repository;

import com.changetype.mscurrency.model.TypeChangeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface TypeChangeRepository  extends JpaRepository<TypeChangeEntity,Integer> {

    @Query("SELECT e FROM TypeChangeEntity e ")
    List<TypeChangeEntity> getAllTypeChange();

    @Query("SELECT e FROM TypeChangeEntity e WHERE e.idTypeChange=:idTypeChange ")
    Optional<TypeChangeEntity> findByIdTypeChange(
            @Param("idTypeChange") Integer idTypeChange);




    @Query(value="SELECT e from TypeChangeEntity e "
            + " where  e.originCurrency=:originCurrency" +
            " and e.destinationCurrency =:destinationCurrency")
    Optional<TypeChangeEntity> getchangeType (
             @Param("originCurrency") String originCurrency,
             @Param("destinationCurrency") String destinationCurrency);

}
