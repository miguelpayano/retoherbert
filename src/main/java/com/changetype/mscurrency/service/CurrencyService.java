package com.changetype.mscurrency.service;


import com.changetype.mscurrency.expose.request.TypeChangeRequest;
import com.changetype.mscurrency.expose.request.UpdateChangeTypeRequest;
import com.changetype.mscurrency.expose.response.PatchTypeChangeResponse;
import com.changetype.mscurrency.expose.response.TypeChangeResponse;
import com.changetype.mscurrency.expose.response.UpdateChangeTypeResponse;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

public interface CurrencyService {

    Observable<UpdateChangeTypeResponse> getAllTypeChange();

    Single<UpdateChangeTypeResponse> findByIdTypeChange(Integer idTypeChange);

    Single<UpdateChangeTypeResponse> updateTypeChangeById(UpdateChangeTypeRequest updateChangeTypeRequest);

    Single<PatchTypeChangeResponse> patchTypeChangeById(UpdateChangeTypeRequest updateChangeTypeRequest);

    Single<UpdateChangeTypeResponse> postTypeCurrencyChange(UpdateChangeTypeRequest updateChangeTypeRequest) throws Exception ;

    Single<TypeChangeResponse> postTypeChange(TypeChangeRequest typeChangeRequest);

}
