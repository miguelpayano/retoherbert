package com.changetype.mscurrency.service;

import com.changetype.mscurrency.expose.request.TypeChangeRequest;
import com.changetype.mscurrency.expose.request.UpdateChangeTypeRequest;
import com.changetype.mscurrency.expose.response.PatchTypeChangeResponse;
import com.changetype.mscurrency.expose.response.TypeChangeResponse;
import com.changetype.mscurrency.expose.response.UpdateChangeTypeResponse;
import com.changetype.mscurrency.model.TypeChangeEntity;
import com.changetype.mscurrency.repository.TypeChangeRepository;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Optional;

@Service
@Slf4j
public class CurrencyServiceImpl  implements CurrencyService{

    @Autowired
    private TypeChangeRepository typeChangeRepository;


    private UpdateChangeTypeResponse convertTypeChangeEntity(TypeChangeEntity typeChangeEntity) {

        UpdateChangeTypeResponse updateChangeTypeResponse = new UpdateChangeTypeResponse();

            updateChangeTypeResponse.setIdTypeChange(typeChangeEntity.getIdTypeChange());
            updateChangeTypeResponse.setDescription(typeChangeEntity.getDescription());
            updateChangeTypeResponse.setOriginCurrency(typeChangeEntity.getOriginCurrency());
            updateChangeTypeResponse.setDestinationCurrency(typeChangeEntity.getDestinationCurrency());
            updateChangeTypeResponse.setChangeType(typeChangeEntity.getChangeType());


        return updateChangeTypeResponse;
    }


    @Override
    public Observable<UpdateChangeTypeResponse> getAllTypeChange() {
        return Observable.just(typeChangeRepository.getAllTypeChange()).flatMapIterable(x -> x)
                .map(this::convertTypeChangeEntity);
    }

    @Override
    public Single<UpdateChangeTypeResponse> findByIdTypeChange(Integer idTypeChange) {

        UpdateChangeTypeResponse changeTypeResponse = null;
        Optional<TypeChangeEntity> optTypeChangeEntity = typeChangeRepository.findByIdTypeChange(idTypeChange);

        if(optTypeChangeEntity.isPresent()){
            changeTypeResponse = convertTypeChangeEntity(optTypeChangeEntity.get());
        }else {
            return null;
        }

        return Single.just(changeTypeResponse);
    }

    @Override
    public Single<UpdateChangeTypeResponse> updateTypeChangeById(UpdateChangeTypeRequest updateChangeTypeRequest) {
        Optional<TypeChangeEntity> optTypeChangeEntity = typeChangeRepository
                .findByIdTypeChange(updateChangeTypeRequest.getIdTypeChange());
        if(optTypeChangeEntity.isPresent()){
            optTypeChangeEntity.get().setDescription(updateChangeTypeRequest.getDescription());
            optTypeChangeEntity.get().setOriginCurrency(updateChangeTypeRequest.getOriginCurrency());
            optTypeChangeEntity.get().setDestinationCurrency(updateChangeTypeRequest.getDestinationCurrency());
            optTypeChangeEntity.get().setChangeType(updateChangeTypeRequest.getChangeType());
        }
        return Single.just(typeChangeRepository
                .save(optTypeChangeEntity
                        .get()))
                .map(this::convertTypeChangeEntity);
    }

    @Override
    public Single<PatchTypeChangeResponse> patchTypeChangeById(UpdateChangeTypeRequest updateChangeTypeRequest) {

        PatchTypeChangeResponse patchTypeChangeResponse = new PatchTypeChangeResponse();

        Optional<TypeChangeEntity> optTypeChangeEntity = typeChangeRepository
                .findByIdTypeChange(updateChangeTypeRequest.getIdTypeChange());

        if(optTypeChangeEntity.isPresent()){
            optTypeChangeEntity.get().setChangeType(updateChangeTypeRequest.getChangeType());

            patchTypeChangeResponse.setCodigo(200L);
            patchTypeChangeResponse.setMessage("Se Actualizo con Exito..!");
        }

        return Single.just(patchTypeChangeResponse);

    }

    @Override
    public Single<UpdateChangeTypeResponse> postTypeCurrencyChange(UpdateChangeTypeRequest updateChangeTypeRequest) throws Exception {

       return Single.just(typeChangeRepository.save(this.convertUpdateChangeTypeRequest(updateChangeTypeRequest)))
                .map(this::convertTypeChangeEntity);
    }

    @Override
    public Single<TypeChangeResponse> postTypeChange(TypeChangeRequest typeChangeRequest) {

        TypeChangeEntity typeChange;
        TypeChangeResponse typeChangeResponse = null;

        Optional<TypeChangeEntity> optChangeType = typeChangeRepository.getchangeType(
               typeChangeRequest.getOriginCurrency(),
               typeChangeRequest.getDestinationCurrency());

       if(optChangeType.isPresent()){
             typeChange = new TypeChangeEntity();

            typeChange.setOriginCurrency(typeChangeRequest.getOriginCurrency());
            typeChange.setDestinationCurrency(typeChangeRequest.getDestinationCurrency());
            typeChange.setChangeType(optChangeType.get().getChangeType());

             typeChangeResponse = toTypeChangeEntity(typeChange);

           typeChangeResponse.setAmount(typeChangeRequest.getAmount());
           typeChangeResponse.setDescription(optChangeType.get().getDescription());
           typeChangeResponse.setAmountWithExchangeRate(
                   typeChange.getChangeType()
                           .multiply(typeChangeRequest.getAmount()));


       }

        return Single.just(typeChangeResponse);


    }


    private TypeChangeEntity convertUpdateChangeTypeRequest(UpdateChangeTypeRequest updateChangeTypeRequest) throws Exception {
        TypeChangeEntity typeChangeEntity = null;

       Optional<TypeChangeEntity> optTypeChangeExist = typeChangeRepository.getchangeType(
                updateChangeTypeRequest.getOriginCurrency(),updateChangeTypeRequest.getDestinationCurrency());

        if(!optTypeChangeExist.isPresent()){
            typeChangeEntity = new TypeChangeEntity();
            typeChangeEntity.setDescription(updateChangeTypeRequest.getDescription());
            typeChangeEntity.setOriginCurrency(updateChangeTypeRequest.getOriginCurrency());
            typeChangeEntity.setDestinationCurrency(updateChangeTypeRequest.getDestinationCurrency());
            typeChangeEntity.setChangeType(updateChangeTypeRequest.getChangeType());
            typeChangeRepository.save(typeChangeEntity);

        }
        return typeChangeEntity;

    }

    private TypeChangeResponse toTypeChangeEntity(TypeChangeEntity typeChangeEntity) {

        TypeChangeResponse typeChangeResponse = new TypeChangeResponse();
        BeanUtils.copyProperties(typeChangeEntity, typeChangeResponse);
        typeChangeResponse.setOriginCurrency(typeChangeEntity.getOriginCurrency());
        typeChangeResponse.setDestinationCurrency(typeChangeEntity.getDestinationCurrency());
        typeChangeResponse.setAmount(typeChangeEntity.getChangeType());
        return typeChangeResponse;

    }



}
